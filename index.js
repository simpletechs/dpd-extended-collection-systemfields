var systemProps = [
  {
    name: 'createdDate',
    type: 'number',
    value: function(item, create, ctx) {
      return new Date().getTime();
    },
    shouldUpdate: function(item, create, ctx) {
      return create;
    }
  },
  {
    name: 'lastModifiedDate',
    type: 'number',
    value: function(item, create, ctx) {
      return new Date().getTime();
    },
    shouldUpdate: function(item, create, ctx) {
      return true;
    }
  },
  {
    name: 'createdBy',
    type: 'string',
    value: function(item, create, ctx) {
      return (ctx.session.isRoot?
                '(root)'
                :(ctx.session.user?
                    ctx.session.user.id
                    :'(anonymous)'
                )
              );
    },
    // only update on create
    // and if !(userEditable && prop set)
    shouldUpdate: function(item, create, ctx, collectionProp) {
      return create && !(collectionProp.userEditable && item.createdBy);
    }
  },
  {
    name: 'lastModifiedBy',
    type: 'string',
    value: function(item, create, ctx) {
      return (ctx.session.isRoot?
                '(root)'
                :(ctx.session.user?
                    ctx.session.user.id
                    :'(anonymous)'
                )
              );
    },
    shouldUpdate: function(item, create, ctx, collectionProp) {
      return true && !(collectionProp.userEditable && item.lastModifiedBy);
    }
  }
];
(function(ExtendedCollection) {
  var _validate = ExtendedCollection.prototype.validate;

  ExtendedCollection.prototype.validate = function(body, create, ctx) {
    var res = _validate.apply(this, arguments);

    var collectionProps = this.properties;
    systemProps.forEach(function(systemProp) {
      // we may only set properties that the user created
      if(!collectionProps[systemProp.name] || !collectionProps[systemProp.name].type == systemProp.type) {
        return;
      }

      // we may not want to set every property
      if(!systemProp.shouldUpdate(body, create, ctx, collectionProps[systemProp.name])) {
        return;
      }

      body[systemProp.name] = systemProp.value(body, create, ctx);
    });

    return res;
  };
})(require('../dpd-extended-collection/index'));
